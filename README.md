# Versioning Data and Models
[ML REPA Library](https://mlrepa.github.io/mlrepa-library/)

## 1. Create and activate virtual environment

Create virtual environment named `dvc-venv` (you may use other name)
```bash
python3 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
```

    
## 2. Add Virtual Environment to Jupyter Notebook

```bash
python -m ipykernel install --user --name=dvc-2-data-versioning
``` 

## 3. Run tutorial `dvc-2-data-versioning.ipynb`:

```bash
jupyter lab
```
